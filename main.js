export { }

import { decodeBlurHash } from "./blurhash.js";

// const accountId = "109536404931347155"; //my account, yay
// const accountStatusEndpoint = `https://mastodon.gamedev.place/api/v1/accounts/${accountId}/statuses`;
const publicTimelineEndpoint = "https://mastodon.gamedev.place/api/v1/timelines/public";
const homeTimelineEndpoint = "https://mastodon.gamedev.place/api/v1/timelines/home";
const appsEndpoint = "https://mastodon.gamedev.place/api/v1/apps";
const tokenEndpoint = "https://mastodon.gamedev.place/oauth/token";
const authorizeEndpoint = "https://mastodon.gamedev.place/oauth/authorize";

class Ui {
    /** @type {HTMLElement} */ mainView;
    /** @type {HTMLAnchorElement} */ loadMoreLink;
    /** @type {HTMLTemplateElement} */ postTemplate;
    /** @type {HTMLTemplateElement} */ postStatsTemplate;
    /** @type {HTMLInputElement} */ oAuthLoginInput;
    /** @type {HTMLButtonElement} */ oAuthLoginButton;

    static mainViewId = "main-view";
    static postTemplateId = "post-template";
    static postStatsId = "post-stats";
    static loadMoreLinkId = ".load-next-batch";
    static oAuthInputId = "oauth-login-input";
    static oAuthButtonId = "oauth-login-button";

    constructor() {
        const mainView = document.getElementById(Ui.mainViewId);
        const loadMoreLink = document.querySelector(Ui.loadMoreLinkId);
        const postTemplate = document.getElementById(Ui.postTemplateId);
        const postStatsTemplate = document.getElementById(Ui.postStatsId);
        const oAuthLoginInput = document.getElementById(Ui.oAuthInputId);
        const oAuthLoginButton = document.getElementById(Ui.oAuthButtonId);

        //looks sorta wasteful, but makes for immediately useful error messages, so not really.
        if (!mainView) throw new Error(`${Ui.mainViewId} not found`);
        if (!loadMoreLink) throw new Error(`${Ui.loadMoreLinkId} not found`);
        if (!postTemplate) throw new Error(`${Ui.postTemplateId} not found`);
        if (!postStatsTemplate) throw new Error(`${Ui.postStatsId} not found`);
        if (!oAuthLoginInput) throw new Error(`${Ui.oAuthInputId} not found`);
        if (!oAuthLoginButton) throw new Error(`${Ui.oAuthButtonId} not found`);

        this.mainView = mainView;
        this.loadMoreLink = /** @type {HTMLAnchorElement} */ (loadMoreLink);
        this.postTemplate = /** @type {HTMLTemplateElement} */ (postTemplate);
        this.postStatsTemplate = /** @type {HTMLTemplateElement} */ (postStatsTemplate);
        this.oAuthLoginInput = /** @type {HTMLInputElement} */ (oAuthLoginInput);
        this.oAuthLoginButton = /** @type {HTMLButtonElement} */ (oAuthLoginButton);
    }
}

//we use this lil guy to convert blurhashes into img instead of using canvas for display,
//so we get alt text for free. It used to be OffscreenCanvas, but Epiphany (based on WebKit)
//doesn't support it. :/
const canvas = document.createElement("canvas");

try {
    const ui = new Ui();
    let url = "";

    //Get query params from current URL, then use them for Mastodon API call
    const searchParams = new URLSearchParams(document.location.search);
    if (searchParams.has("code")) {
        //this is a redirect and we wanna display the user's home timeline
        searchParams.delete("code");
        searchParams.set("limit", "40");
        url = `${homeTimelineEndpoint}?${searchParams.toString()}`;
        console.log("Yo this is a redirect, load the home timeline");

        const tokenStr = window.sessionStorage.getItem("sk_user_token");
        if (tokenStr) {
            const token = JSON.parse(tokenStr);
            const homeTimelineResponse = await fetchTimeline(url, token);
            if (homeTimelineResponse.ok) {
                const statusData = await homeTimelineResponse.json();
                await loadPostsAsync(ui.mainView, statusData, ui);
            } else {
                console.error("something went wrong loading posts :/");
            }
        } else console.warn("got no token, did we forget to save?");
    } else {
        searchParams.set("limit", "40");

        url = `${publicTimelineEndpoint}?${searchParams.toString()}`;
        const publicTimelineResponse = await fetchTimeline(url, null);
        if (publicTimelineResponse.ok) {
            const statusData = await publicTimelineResponse.json();
            await loadPostsAsync(ui.mainView, statusData, ui);
        } else {
            console.error("something went wrong loading posts :/");
        }

        const devModeVal = localStorage.getItem("sk_dev");

        if (Number(devModeVal) === 1) {
            url = "dev_data/dev_data.json";
        }
    }
} catch (error) {
    console.error(`Something went wrong: ${error}`);
}

/**
 * 
 * @param {string} url 
 * @param {Object} token 
 * @returns {Promise<Response>}
 */
async function fetchTimeline(url, token) {
    let options = {};

    if (token) {
        options.headers = {
            "Authorization": `Bearer ${token["access_token"]}`
        }
    }

    return fetch(url, options);
}

/**
 * 
 * @param {HTMLElement} mainView 
 * @param {any[]} statusArray 
 * @param {Ui} ui
 */
async function loadPostsAsync(mainView, statusArray, ui) {
    createMainView(statusArray, mainView, ui);
    // const response = await fetchTimeline(url, null);

    // if (response.ok) {
    //     const json = await response.json();
    //     createMainView(json, mainView, ui);
    // } else {
    //     console.error("something went wrong loading posts :/");
    //     ui.loadMoreLink.className = "load-next-batch";
    // }

    await registerApplication();

    ui.oAuthLoginButton.addEventListener("click", ev => {
        const oAuthCode = ui.oAuthLoginInput.value;

        const applicationStr = window.sessionStorage.getItem("sk_registered_application");
        if (applicationStr) {
            const application = JSON.parse(applicationStr);
            const clientId = application["client_id"];
            const clientSecret = application["client_secret"];
            authorizeUser(clientId, clientSecret, oAuthCode);
        } else {
            console.warn("application shouldn't be null, yo. not performing login");
        }
    });
}

async function registerApplication() {
    const params = new URLSearchParams();
    params.append("client_name", "skeleton");
    params.append("redirect_uris", "urn:ietf:wg:oauth:2.0:oob");
    params.append("scopes", "read write push");
    const url = `${appsEndpoint}?${params.toString()}`;

    const response = await fetch(url, {
        method: "POST"
    });

    if (response.ok) {
        const application = await response.json();
        console.log(application);

        window.sessionStorage.setItem("sk_registered_application", JSON.stringify(application));

        const clientId = application["client_id"];
        const clientSecret = application["client_secret"];

        try {
            await getOAuthToken(clientId, clientSecret);
            await verifyCredentials();
            printLoginUrl(clientId);
        } catch (error) {
            console.error(error);
        }
    }
}

/**
 * @param {string} clientId
 * @param {string} clientSecret
 * @param {string} loginCode
 */
async function authorizeUser(clientId, clientSecret, loginCode) {
    const params = new URLSearchParams();
    params.append("client_id", clientId);
    params.append("client_secret", clientSecret);
    params.append("redirect_uri", "urn:ietf:wg:oauth:2.0:oob");
    params.append("grant_type", "authorization_code");
    params.append("code", loginCode);
    params.append("scope", "read write push");

    const url = `${tokenEndpoint}?${params.toString()}`;

    const response = await fetch(url, {
        method: "POST"
    });

    if (response.ok) {
        const token = await response.json();
        console.log(token);

        //TODO: save in storage api, then use it for e.g. the logged in user timeline
        window.sessionStorage.setItem("sk_user_token", JSON.stringify(token));

        //navigate to self with code param (will be later used for redirection I guess),
        //so we know we're logged in on load and then load the home timeline instead of public.
        const searchParams = new URLSearchParams();
        searchParams.set("limit", "40");
        searchParams.set("code", "");
        window.location.href = `?${searchParams.toString()}`;

        // const homeTimelineResponse = await fetch(homeTimelineEndpoint, {
        //     headers: {
        //         "Authorization": `Bearer ${token["access_token"]}`
        //     }
        // });
        // if (homeTimelineResponse.ok) {
        //     const statusData = await homeTimelineResponse.json();
        //     console.log(statusData);
        // } else console.warn("failed to fetch home timeline :/");
    } else {
        console.error("something went wrong during login");
    }
}

/**
 * @param {string} clientId
 */
function printLoginUrl(clientId) {
    const params = new URLSearchParams();
    params.append("client_id", clientId);
    params.append("scope", "read write push");
    params.append("redirect_uri", "urn:ietf:wg:oauth:2.0:oob");
    params.append("response_type", "code");
    const url = `${authorizeEndpoint}?${params.toString()}`;

    //alright, so this can't be a fetch it seems, we just navigate to the server, login via their form
    //and then get back a code which we can use to obtain another oauth token that enables us to post toots.
    //so now redirect_url makes sense, it'd pass us code as a parameter then and we could go on with oauth shit.
    console.log("login url: " + url);
}

/**
 * 
 * @param {string} clientId 
 * @param {string} clientSecret 
 */
async function getOAuthToken(clientId, clientSecret) {
    const params = new URLSearchParams();
    params.append("client_id", clientId);
    params.append("client_secret", clientSecret);
    params.append("redirect_uri", "urn:ietf:wg:oauth:2.0:oob");
    params.append("grant_type", "client_credentials");
    const url = `${tokenEndpoint}?${params.toString()}`;

    const response = await fetch(url, {
        method: "POST"
    });

    if (response.ok) {
        const token = await response.json();
        console.log(token);

        const storage = window.sessionStorage;
        storage.setItem("sk_oauth_token", JSON.stringify(token));
    }
}

async function verifyCredentials() {
    const storage = window.sessionStorage;
    const tokenString = storage.getItem("sk_oauth_token");
    const token = JSON.parse(tokenString);

    if (token) {
        const verifyEndpoint = "https://mastodon.gamedev.place/api/v1/apps/verify_credentials";
        const verifyResponse = await fetch(verifyEndpoint, {
            headers: {
                "Authorization": `Bearer ${token["access_token"]}`
            }
        });

        if (verifyResponse.ok) {
            const app = await verifyResponse.json();
            console.log(`Looks like verify ran fine: ${JSON.stringify(app)}`);
        }
    } else {
        console.warn("yo, token seems to be missing");
    }
}

/**
 * 
 * @param {Array} statusArray 
 * @param {HTMLElement} mainView 
 * @param {Ui} ui
 */
async function createMainView(statusArray, mainView, ui) {
    ui.loadMoreLink.className = "load-next-batch";

    console.log(statusArray);

    const fragment = document.createDocumentFragment();

    for (let status of statusArray) {
        const reblog = status["reblog"];
        if (reblog !== null) status = reblog;

        const userName = status["account"]["username"];
        const fullAccountName = status["account"]["acct"];
        const avatar = status["account"]["avatar_static"]; //avatars can be gifs too apparently

        const templateClone = /**@type {HTMLElement} */ (ui.postTemplate.content.cloneNode(true));

        const avatarImgNode = /**@type {HTMLImageElement} */ (templateClone.querySelector(".post-avatar"));
        avatarImgNode.src = avatar;

        const userNameNode = /** @type {HTMLElement} */ (templateClone.querySelector(".friendly-name"));
        userNameNode.textContent = userName;

        const fullAccountNameNode = /** @type {HTMLElement} */ (templateClone.querySelector(".full-account-name"));
        fullAccountNameNode.textContent = fullAccountName;

        const sensitive = status["sensitive"];
        const content = status["content"];
        const contentNode = createContentNode(sensitive, status, fragment, content);

        const statusId = status["id"];
        const articleNode = /** @type {HTMLElement} */ (templateClone.firstElementChild); //probably better to use a class here instead
        articleNode.setAttribute("status-id", statusId);
        articleNode.addEventListener("click", onStatusClicked);
        articleNode.appendChild(contentNode);

        const mediaAttachments = status["media_attachments"];
        appendMediaAttachments(mediaAttachments, contentNode, sensitive);

        const reblogsCount = status["reblogs_count"];
        const repliesCount = status["replies_count"];
        const favoritesCount = status["favourites_count"];
        const creationDate = status["created_at"];
        const statsNode = createStatsNode(creationDate, repliesCount, reblogsCount, favoritesCount, ui);
        articleNode.appendChild(statsNode);

        fragment.append(templateClone);
    }

    const lastIndex = statusArray.length - 1;
    let id = 0;
    if (lastIndex >= 0) {
        id = statusArray[lastIndex]["id"];
    }
    setupLoadNextLink(ui.loadMoreLink, id);

    mainView.append(fragment);
}

/**
 * 
 * @param {string} creationDate 
 * @param {number} repliesCount 
 * @param {number} reblogsCount 
 * @param {number} favoritesCount 
 * @param {Ui} ui
 */
function createStatsNode(creationDate, repliesCount, reblogsCount, favoritesCount, ui) {
    const statsNode = /** @type {HTMLElement} */ (ui.postStatsTemplate.content.cloneNode(true));
    const innerP = /** @type {HTMLParagraphElement} */ (statsNode.firstElementChild);
    innerP.addEventListener("click", onStatsControlsClicked);

    const repliesNode = /** @type {HTMLElement} */ (innerP.children[0]);
    repliesNode.innerText = `↩: ${repliesCount}`;

    const reblogsNode = /** @type {HTMLElement} */ (innerP.children[1]);
    reblogsNode.innerText = `🔁: ${reblogsCount}`;

    const favoritesNode = /** @type {HTMLElement} */ (innerP.children[2]);
    favoritesNode.innerText = `⭐: ${favoritesCount}`;

    const timeNode = /** @type {HTMLTimeElement} */ (innerP.children[3]);
    const nicerDate = new Date(creationDate).toLocaleString();
    timeNode.dateTime = nicerDate;
    timeNode.innerHTML = `<br> ${nicerDate}`;

    return statsNode;
}

/**
 * 
 * @param {boolean} sensitive 
 * @param {Object} status 
 * @param {DocumentFragment} fragment 
 * @param {string} content 
 * @returns Node that directly contains the content HTML.
 */
function createContentNode(sensitive, status, fragment, content) {
    const p = document.createElement("p");
    fragment.append(p);

    if (sensitive) {
        //In this case the content shouldn't be under the p node created above, but on
        //the details one so that attachments are collapsed with the post text.
        const cw = status["spoiler_text"];
        const summaryNode = document.createElement("summary");
        summaryNode.textContent = cw;

        const detailsNode = document.createElement("details");
        detailsNode.appendChild(summaryNode);
        detailsNode.insertAdjacentHTML("beforeend", content);

        p.appendChild(detailsNode);

    } else {
        p.innerHTML = content;
    }

    return p;
}

/**
 * 
 * @param {Array} mediaAttachmentsObj 
 * @param {HTMLElement} contentNode 
 * @param {boolean} sensitive 
 */
function appendMediaAttachments(mediaAttachmentsObj, contentNode, sensitive) {
    if (mediaAttachmentsObj.length > 0) {
        if (sensitive) {
            //Sensitive content uses details/summary, so media attachments should be children of
            //that so they're collapsible, but contentNode we get is the enclosing paragraph.
            contentNode = contentNode.querySelector("details");
        }

        const firstAttachment = mediaAttachmentsObj[0];
        const type = firstAttachment["type"];
        const url = firstAttachment["url"];

        if (type === "image") {
            const meta = firstAttachment["meta"]["small"];
            const width = meta["width"];
            const height = meta["height"];

            const imageNode = document.createElement("img");
            imageNode.className = "post-img";
            imageNode.width = width;
            imageNode.height = height;
            imageNode.loading = "lazy";

            //preview_url seems to get a smaller version of the image
            const imageUrl = firstAttachment["preview_url"];

            const description = firstAttachment["description"];
            if (description) {
                imageNode.alt = description;
            }

            if (sensitive) {
                //we save the image URL here so we can swap the blurhash'd image for the real thing on click
                imageNode.setAttribute("sk-swap-url", imageUrl);
                imageNode.addEventListener("click", onSensitiveClicked);

                const blurhash = firstAttachment["blurhash"];
                const pixelData = decodeBlurHash(blurhash, width, height, null);
                const imageData = new ImageData(pixelData, width, height);
                canvas.width = width;
                canvas.height = height;

                // @ts-ignore
                canvas.getContext("2d").putImageData(imageData, 0, 0);
                imageNode.src = canvas.toDataURL();
            } else {
                imageNode.src = imageUrl;
            }

            contentNode.appendChild(imageNode);
        } else if (type === "video") {
            const videoNode = document.createElement("video");
            videoNode.className = "post-video";
            videoNode.src = url;
            videoNode.controls = true;

            const posterUrl = firstAttachment["preview_url"];
            if (posterUrl) {
                videoNode.poster = posterUrl;
            }

            if (sensitive) {
                console.log("sensitive video!");
            }

            contentNode.appendChild(videoNode);
        } else if (type === "gifv") {
            const videoNode = document.createElement("video");
            videoNode.className = "post-video";
            videoNode.src = url;
            videoNode.controls = true;
            videoNode.loop = true;
            //video doesn't have an alt attribute, so we need to handle that some other way...

            if (sensitive) {
                console.log("sensitive gifv!");
            }

            contentNode.appendChild(videoNode);
        } else if (type === "audio") {
            const audioNode = document.createElement("audio");
            audioNode.controls = true;
            audioNode.src = url;

            contentNode.appendChild(audioNode);
        }
        else {
            console.warn(`dunno how to handle attachment of type ${type}. Attachment: ${JSON.stringify(firstAttachment)}`);
        }
    }
}

/**
 * Blurs/unblurs sensitive images when clicked.
 * @param {MouseEvent} event 
 */
function onSensitiveClicked(event) {
    const sensitiveElement = /** @type {HTMLElement} */ (event.currentTarget);
    if (sensitiveElement) {
        const swapUrl = sensitiveElement.getAttribute("sk-swap-url");
        const currentUrl = sensitiveElement.getAttribute("src");
        // @ts-ignore
        sensitiveElement.setAttribute("src", swapUrl);
        // @ts-ignore
        sensitiveElement.setAttribute("sk-swap-url", currentUrl);

        event.stopPropagation();
    }
}

/**
 * 
 * @param {HTMLAnchorElement} loadMoreLink 
 * @param {number} lastLoadedPost 
 */
function setupLoadNextLink(loadMoreLink, lastLoadedPost) {
    const searchParams = new URLSearchParams(document.location.search);
    searchParams.set("max_id", lastLoadedPost.toString());
    searchParams.set("limit", "40");
    loadMoreLink.href = `?${searchParams.toString()}`;
}

/**
 * Runs when a single status (post) is clicked.
 * @param {MouseEvent} event 
 */
async function onStatusClicked(event) {
    const articleNode = /** @type {HTMLElement} */ (event.currentTarget);
    if (articleNode !== null) {
        //tbh, I wonder if this is dumb vs just keeping around the JSON we fetched in some global.
        //then again, this is a fun/learning project and not a "we need to be done 2 months ago" kind of thing,
        //so who knows. Maybe we'll find out later, maybe we won't.
        const statusId = articleNode.getAttribute("status-id");
        if (statusId) {
            const url = `https://mastodon.gamedev.place/api/v1/statuses/${statusId}/context`;
            const response = await fetch(url);
            if (response.ok) {
                const status = await response.json();
                console.log(status);
            } else {
                console.error(`error fetching post ${statusId}: ${response.statusText}`);
            }
        } else console.warn(`no status id found for ${articleNode}, did we forget to assign?`);
    }
}

/**
 * 
 * @param {MouseEvent} ev 
 */
function onStatsControlsClicked(ev) {
    ev.stopPropagation();

    console.log(`You wanna interact with this post?
I'm not sure I'll ever get this implemented.
How about using the official web client instead?`);

    //TODO: play with popover API
}
